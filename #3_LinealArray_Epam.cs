// Third task
using System;
using System.Text;

namespace EpamTasks
{	
	class LinealArray
	{
		private Array _data;
		private int _startIndex;
		private int _endIndex;
		
		public LinealArray(int startIndex, int endIndex)
		{
			
			_startIndex = startIndex;
			_endIndex = endIndex;
			
			_data = Array.CreateInstance(typeof(int), new int[1] { endIndex }, new int[1] { startIndex });
		}
		
		public LinealArray(int startIndex, int endIndex, int[] arr)
		{
			if (endIndex - startIndex + 1 != arr.Length)
			{
				Console.WriteLine("Error");
				return;
			}
			
			_startIndex = startIndex;
			_endIndex = endIndex;
			
			_data = Array.CreateInstance(typeof(int), new int[1] { endIndex }, new int[1] { startIndex });
			
			int j = 0;
			for (int i = startIndex; i <= endIndex; i++)
			{
				_data.SetValue(arr[j++], i);
			}
		}
		
		public int Length
		{
			get { return _endIndex - _startIndex + 1; }
		}
		
		public override string ToString()
		{
			// Use StringBuilder when there are many concatenation to improve performance
			StringBuilder s = new StringBuilder();
			
			for (int i = _startIndex; i <= _endIndex; i++)
			{
				s.Append(String.Format("{0} ", _data.GetValue(i)));
			}
			return s.ToString();
		}
		
		public int this [int index]
		{

            set 
			{ 
				if (index > _endIndex || index < _startIndex)
				{
					throw new IndexOutOfRangeException();
				}
				_data.SetValue(value, index); 
			}
            get 
			{ 
				return (int)_data.GetValue(index); 
			}

        }
		
		public static LinealArray Addition(LinealArray array1, LinealArray array2)
		{
			if (!CheckRange(array1, array2))
			{
				throw new Exception("There are different index ranges in arrays.");
			}
			
			int[] arr = new int[array1.Length];
			int start = array1._startIndex;
			int end = array1._endIndex;
			int j = 0;
			
			for (int i = start; i <= end; i++)
			{
				arr[j++] = array1[i] + array2[i];
			}
			
			return new LinealArray(start, end, arr);
		}
		
		public static LinealArray Subtraction(LinealArray array1, LinealArray array2)
		{
			if (!CheckRange(array1, array2))
			{
				throw new Exception("There are different index ranges in arrays.");
			}
			
			int[] arr = new int[array1.Length];
			int start = array1._startIndex;
			int end = array1._endIndex;
			int j = 0;
			
			for (int i = start; i <= end; i++)
			{
				arr[j++] = array1[i] - array2[i];
			}
			
			return new LinealArray(start, end, arr);
		}
		
		public void Multiply(int number)
		{			
			for (int i = this._startIndex; i <= this._endIndex; i++)
			{
				this[i] = this[i] * number;
			}
		}
		
		public override bool Equals(object obj)
		{
			// If parameter is null return false to avoid unnecessary step with creating new array
			if (obj == null)
			{
				return false;
			}
			
			LinealArray other = (LinealArray)obj;

			if (other == null)
			{
				return false;
			}
			
			if (!CheckRange(this, other))
			{
				return false;
			}
			
			for (int i = this._startIndex; i <= this._endIndex; i++)
			{
				if (this[i] != other[i])
				{
					return false;
				}
			}
			
			return true;
		}
		
		// Equals for own types to improve performance
		public bool Equals(LinealArray other)
		{

			if ((object)other == null)
			{
				return false;
			}
			
			if (!CheckRange(this, other))
			{
				return false;
			}
			
			for (int i = this._startIndex; i <= this._endIndex; i++)
			{
				if (this[i] != other[i])
				{
					return false;
				}
			}
			
			return true;
		}
		
		public static bool Equals( LinealArray array1, LinealArray array2)
		{
			// If both are null, or both are same instance, return true
			if (Object.ReferenceEquals(array1, array2))
			{
				return true;
			}

			// If one is null, but not both, return false
			if (((object)array1 == null) || ((object)array2 == null))
			{
				return false;
			}
			
			if (!CheckRange(array1, array2))
			{
				return false;
			}
			
			for (int i = array1._startIndex; i <= array2._endIndex; i++)
			{
				if (array1[i] != array2[i])
				{
					return false;
				}
			}
			
			return true;
		}
		
		public override int GetHashCode()
		{
			return (int)_data.GetValue(_startIndex) ^ (int)_data.GetValue(_endIndex);
		}
		
		public static LinealArray operator +(LinealArray array1, LinealArray array2)
		{
			if (!CheckRange(array1, array2))
			{
				throw new Exception("There are different index ranges in arrays.");
			}
			
			int[] arr = new int[array1.Length];
			int start = array1._startIndex;
			int end = array1._endIndex;
			int j = 0;
			
			for (int i = start; i <= end; i++)
			{
				arr[j++] = array1[i] + array2[i];
			}
			
			return new LinealArray(start, end, arr);
		}
		
		public static LinealArray operator -(LinealArray array1, LinealArray array2)
		{
			if (!CheckRange(array1, array2))
			{
				throw new Exception("There are different index ranges in arrays.");
			}
			
			int[] arr = new int[array1.Length];
			int start = array1._startIndex;
			int end = array1._endIndex;
			int j = 0;
			
			for (int i = start; i <= end; i++)
			{
				arr[j++] = array1[i] - array2[i];
			}
			
			return new LinealArray(start, end, arr);
		}
		
		public static LinealArray operator *(LinealArray array, int number)
		{
			int[] tmpArray = new int[array.Length];
			int j = 0;
			
			for (int i = array._startIndex; i <= array._endIndex; i++)
			{
				tmpArray[j++] = array[i] * number;
			}
			
			return new LinealArray(array._startIndex, array._endIndex, tmpArray);
		}
		
		public static bool operator ==(LinealArray array1, LinealArray array2)
		{
			return Equals(array1, array2);
		}

		public static bool operator !=(LinealArray array1, LinealArray array2)
		{
			return !Equals(array1, array2);
		}
		
		private static bool CheckRange(LinealArray array1, LinealArray array2)
		{
			if (array1._startIndex != array2._startIndex || array1._endIndex != array2._endIndex)
			{
				return false;
			}
			return true;
		}
	}
	
	class EntryPoint
	{
		static void Main()
		{
			int[] tmp1 = {2, 6, 3, 4, 7, 9, 1, 8};
			int[] tmp2 = {3, 5, 1, 2, 4, 7, 5, 6};
			LinealArray arr1 = new LinealArray(3, 10, tmp1);
			LinealArray arr2 = new LinealArray(3, 10, tmp2);
			LinealArray arr3 = new LinealArray(3, 10, tmp2);
			Console.WriteLine("\n******************************THIRD TASK****************************\n");
			Console.WriteLine("First array: {0}", arr1);
			Console.WriteLine("Second array: {0}\n", arr2);
			Console.WriteLine("Element by index 4 in first array: {0}", arr1[4]);
			Console.WriteLine("First array plus second array: {0}", LinealArray.Addition(arr1, arr2));
			Console.WriteLine("First array minus second array: {0}", arr1 - arr2);
			Console.WriteLine("First array multiplied by 2: {0}", arr1 * 2);
			
			if (arr2 == arr3)
			{
				Console.WriteLine("Second and third are equals");
			}
			if (arr1 != arr2)
			{
				Console.WriteLine("First and second are not equals");
			}
		}
	}
}