// Fourth task
using System;
using System.Text;

namespace EpamTasks
{
	class Matrix
	{
		private double[,] _innerMatrix;
		private int _rowCount = 0;
		private int _columnCount = 0;

		public Matrix(int rowCount, int columnCount)
		{
			_rowCount = rowCount;
			_columnCount = columnCount;
			_innerMatrix = new double[rowCount, columnCount];
		}
		
		public Matrix(int rowCount, int columnCount, double[,] matrix)
		{
			if (rowCount != matrix.GetLength(0) || columnCount != matrix.GetLength(1))
			{
				throw new Exception("Wrong matrix");
			}
			
			_rowCount = rowCount;
			_columnCount = columnCount;
			_innerMatrix = new double[rowCount, columnCount];
			
			for (int i = 0; i < RowCount; i++)
			{
				for (int j = 0; j < ColumnCount; j++)
				{
					_innerMatrix[i, j] = matrix[i, j];
				}
			}
		}
		
		public int RowCount
		{
			get { return _rowCount; }
		}
		
		public int ColumnCount
		{
			get { return _columnCount; }
		}
		
		public double this[int rowNumber,int columnNumber]
		{
			get
			{
				return _innerMatrix[rowNumber, columnNumber];
			}
			set
			{
				_innerMatrix[rowNumber, columnNumber] = value;
			}
		}

		public double[] GetRow(int rowIndex)
		{
			double[] rowValues = new double[_columnCount];
			
			for (int i = 0; i < _columnCount; i++)
			{
				rowValues[i] = _innerMatrix[rowIndex, i];
			}
			
			return rowValues;
		}
		
		public void SetRow(int rowIndex, double[] array)
		{
			if (array.Length != _columnCount)
			{
				throw new Exception("Wrong length of input array");
			}
			
			for (int i = 0; i < array.Length; i++)
			{
				_innerMatrix[rowIndex, i] = array[i];
			}
		}
		
		public double[] GetColumn(int columnIndex)
		{ 
			double[] columnValues = new double[_rowCount];
			
			for (int i = 0; i < _rowCount; i++)
			{
				columnValues[i] = _innerMatrix[i, columnIndex];
			}
			
			return columnValues;
		}
		
		public void SetColumn(int columnIndex,double[] array)
		{ 
			if (array.Length != _rowCount)
			{
				throw new Exception("Wrong length of input array");
			}
			
			for (int i = 0; i < array.Length; i++)
			{
				_innerMatrix[i, columnIndex] = array[i];
			}
		}
		
		public static bool operator ==(Matrix matrix1, Matrix matrix2)
		{
			return Equals(matrix1, matrix2);
		}
		
		public static bool operator !=(Matrix matrix1, Matrix matrix2)
		{
			return !(matrix1 == matrix2);
		}
		
		public static Matrix operator -(Matrix matrix)
		{
			return matrix * (-1);
		}
		
		public static Matrix operator ++(Matrix matrix)
		{
			for (int i = 0; i < matrix.RowCount; i++)
			{
				for (int j = 0; j < matrix.ColumnCount; j++)
				{
					matrix[i, j] += 1;
				}
			}
			return matrix;
		}
		
		public static Matrix operator --(Matrix matrix)
		{
			for (int i = 0; i < matrix.RowCount; i++)
			{
				for (int j = 0; j < matrix.ColumnCount; j++)
				{
					matrix[i, j] -= 1;
				}
			}
			return matrix;
		}
		
		public static Matrix operator *(Matrix matrix1, Matrix matrix2)
		{
			if (matrix1.ColumnCount != matrix2.RowCount)
			{
				throw new Exception("Wrong sizes of matrixes");
			}
			
			Matrix returnMatrix = new Matrix(matrix1.RowCount, matrix2.ColumnCount);
			
			for (int i = 0; i < matrix1.RowCount; i++)
			{
				double[] rowValues = matrix1.GetRow(i);
				
				for (int j = 0; j < matrix2.ColumnCount; j++)
				{
					double[] columnValues = matrix2.GetColumn(j);
					double value = 0;
					
					for (int a = 0; a < rowValues.Length; a++)
					{
						value += rowValues[a] * columnValues[a];
					}
					
					returnMatrix[i, j] = value;
				}
			}
			
			return returnMatrix;
		}
		
		public Matrix Transpose()
		{
			Matrix returnMartix = new Matrix(ColumnCount, RowCount);
			
			for (int i = 0; i < RowCount; i++)
			{
				for (int j = 0; j < ColumnCount; j++)
				{
					returnMartix[j, i] = _innerMatrix[i, j];
				}
			}
			
			return returnMartix;
		}
		
		public override bool Equals(object obj)
		{
			// If parameter is null return false to avoid unnecessary step with creating new matrix
			if (obj == null)
			{
				return false;
			}
			
			Matrix other = (Matrix)obj;

			if (other == null)
			{
				return false;
			}
			
			if (!CheckRange(this, other))
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < this.ColumnCount; j++)
				{
					if (this[i, j] != other[i, j])
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		// Equals for own types to improve performance
		public bool Equals(Matrix other)
		{

			if ((object)other == null)
			{
				return false;
			}
			
			if (!CheckRange(this, other))
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < this.ColumnCount; j++)
				{
					if (this[i, j] != other[i, j])
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		public static bool Equals(Matrix matrix1, Matrix matrix2)
		{
			// If both are null, or both are same instance, return true
			if (Object.ReferenceEquals(matrix1, matrix2))
			{
				return true;
			}

			// If one is null, but not both, return false
			if (((object)matrix1 == null) || ((object)matrix2 == null))
			{
				return false;
			}
			
			if (!CheckRange(matrix1, matrix2))
			{
				return false;
			}
			
			for (int i = 0; i < matrix1.RowCount; i++)
			{
				for (int j = 0; j < matrix1.ColumnCount; j++)
				{
					if (matrix1[i, j] != matrix2[i, j])
					{
						return false;
					}
				}
			}
			
			return true;
		}		
		
		public override int GetHashCode()
		{
			return (int)_innerMatrix[0, 0] ^ (int)_innerMatrix[_rowCount, _columnCount];
		}

		public bool IsZeroMatrix()
		{
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < this.ColumnCount; j++)
				{
					if (_innerMatrix[i, j] != 0)
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		public bool IsSquareMatrix()
		{
			return (this.RowCount == this.ColumnCount);
		}
		
		public bool IsLowerTriangle()
		{
			if (!this.IsSquareMatrix())
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = i + 1; j < this.ColumnCount; j++)
				{
					if (_innerMatrix[i, j] != 0)
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		public bool IsUpperTriangle()
		{
			if (!this.IsSquareMatrix())
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < i; j++)
				{
					if (_innerMatrix[i, j] != 0)
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		public bool IsDiagonalMatrix()
		{
			if (!this.IsSquareMatrix())
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < this.ColumnCount; j++)
				{
					if (i != j && _innerMatrix[i, j] != 0)
					{
						return false;
					}
				}
			}
			
			return true;
		}
		
		public bool IsIdentityMatrix()
		{
			if (!this.IsSquareMatrix())
			{
				return false;
			}
			
			for (int i = 0; i < this.RowCount; i++)
			{
				for (int j = 0; j < this.ColumnCount; j++)
				{
					double checkValue = 0;
					if (i == j)
					{
						checkValue = 1;
					}
					
					if (_innerMatrix[i, j] != checkValue)
					{
						return false;
					}
				}
			}
			
			return true;
		}

		public bool IsSymetricMatrix()
		{
			if (!this.IsSquareMatrix())
			{
				return false;
			}
			
			Matrix transposeMatrix = this.Transpose();
			
			if (this == transposeMatrix)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public Matrix SubMatrix(int startRowIndex, int startColumnIndex, int rowCount, int columnCount)
		{
			if (rowCount > RowCount - startRowIndex || columnCount > ColumnCount - startColumnIndex || rowCount < 1 || columnCount < 1)
			{
				throw new Exception("The number of rows or columns is too large or less than one!");
			}				
			if (startRowIndex < 0 || startColumnIndex < 0 || startRowIndex > RowCount || startColumnIndex > ColumnCount)
			{
				throw new IndexOutOfRangeException("Index row or column is too large or less than zero!");
			}
			
			Matrix returnMatrix = new Matrix(rowCount, columnCount);
			int i = 0, j = 0;
			
			for (int a = startRowIndex; a <= rowCount; a++)
			{
				j = 0;
				for (int b = startColumnIndex; b <= columnCount; b++)
				{
					returnMatrix[i, j] = this[a, b];
					j++;
				}
				i++;
			}
			
			return returnMatrix;
		}
		
		public override string ToString()
		{
			// Use StringBuilder when there are many concatenation to improve performance
			StringBuilder s = new StringBuilder();
			
			s.Append(String.Format("({0}x{1})\n", this.RowCount, this.ColumnCount));
			
			for (int i = 0; i < RowCount; i++)
			{
				for (int j = 0; j < ColumnCount; j++)
				{
					s.Append(String.Format("{0}\t", this[i, j]));
				}
				s.Append("\n");
			}
	
			return s.ToString();
		}
		
		public static Matrix Add(Matrix matrix1, Matrix matrix2)
		{
			if (!(matrix1.RowCount == matrix2.RowCount && matrix1.ColumnCount == matrix2.ColumnCount))
			{
				throw new Exception("Different sizes of matrixes");
			}
			
			Matrix returnMartix = new Matrix(matrix1.RowCount, matrix1.ColumnCount);
			
			for (int i = 0; i < matrix1.RowCount; i++)
			{
				for (int j = 0; j < matrix1.ColumnCount; j++)
				{
					returnMartix[i, j] = matrix1[i, j] + matrix2[i, j];
				}
			}
			return returnMartix;
		}
		
		public static Matrix Subtract(Matrix matrix1, Matrix matrix2)
		{
			if (!(matrix1.RowCount == matrix2.RowCount && matrix1.ColumnCount == matrix2.ColumnCount))
			{
				throw new Exception("Different sizes of matrixes");
			}
			return matrix1 + (matrix2 * (-1));
		}
		
		public static Matrix Multiply(Matrix matrix, double scalarValue)
		{
			Matrix returnMartix = new Matrix(matrix.RowCount, matrix.ColumnCount);
			
			for (int i = 0; i < matrix.RowCount; i++)
			{
				for (int j = 0; j < matrix.ColumnCount; j++)
				{
					returnMartix[i, j] = matrix[i, j] * scalarValue;
				}
			}
			
			return returnMartix;
		}
		
		public static Matrix operator +(Matrix matrix1, Matrix matrix2)
		{
			if (!(matrix1.RowCount == matrix2.RowCount && matrix1.ColumnCount == matrix2.ColumnCount))
			{
				throw new Exception("Different sizes of matrixes");
			}
			
			Matrix returnMartix = new Matrix(matrix1.RowCount, matrix1.ColumnCount);
			
			for (int i = 0; i < matrix1.RowCount; i++)
			{
				for (int j = 0; j < matrix1.ColumnCount; j++)
				{
					returnMartix[i, j] = matrix1[i, j] + matrix2[i, j];
				}
			}
			return returnMartix;
		}
		
		public static Matrix operator -(Matrix matrix1, Matrix matrix2)
		{
			if (!(matrix1.RowCount == matrix2.RowCount && matrix1.ColumnCount == matrix2.ColumnCount))
			{
				throw new Exception("Different sizes of matrixes");
			}
			return matrix1 + (matrix2 * (-1));
		}
		
		public static Matrix operator *(Matrix matrix, double scalarValue)
		{
			Matrix returnMartix = new Matrix(matrix.RowCount, matrix.ColumnCount);
			
			for (int i = 0; i < matrix.RowCount; i++)
			{
				for (int j = 0; j < matrix.ColumnCount; j++)
				{
					returnMartix[i, j] = matrix[i, j] * scalarValue;
				}
			}
			
			return returnMartix;
		}
		
		private static bool CheckRange(Matrix matrix1, Matrix matrix2)
		{
			if (matrix1.RowCount != matrix2.RowCount || matrix1.ColumnCount != matrix2.ColumnCount)
			{
				return false;
			}
			return true;
		}
	}
	
	class EntryPoint
	{
		static void Main()
		{
			double[,] tmp3 = {{5, 1, 4, 3, 2}, {6, 2, 4, 5, 1}, {4, 2, 8, 9, 5}, {3, 0, 2, 7, 4}};
			double[,] tmp4 = {{2, 3, 1, 0, 5}, {7, 3, 5, 4, 3}, {3, 1, 7, 4, 9}, {4, 6, 1, 8, 7}};
			double[,] tmp5 = {{1, 4, 2, 4, 8}, {6, 2, 4, 1, 8}, {7, 6, 9, 2, 4}, {4, 5, 7, 2, 1}, {5, 0, 6, 2, 9}};
			double[,] tmp6 = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
			Matrix matrix1 = new Matrix(4, 5, tmp3);
			Matrix matrix2 = new Matrix(4, 5, tmp4);
			Matrix matrix3 = new Matrix(5, 5, tmp5);
			Matrix matrix4 = new Matrix(4, 5, tmp3);
			Matrix matrix5 = new Matrix(3, 3, tmp6);
			Console.WriteLine("\n******************************FOURTH TASK****************************\n");
			Console.WriteLine("First matrix:\n{0}", matrix1.ToString());
			Console.WriteLine("Second matrix:\n{0}", matrix2.ToString());
			Console.WriteLine("Third matrix:\n{0}", matrix3.ToString());
			Console.WriteLine("Fourth matrix:\n{0}", matrix4.ToString());
			Console.WriteLine("Fifth matrix:\n{0}", matrix5.ToString());
			Console.WriteLine("First + second:\n{0}", (matrix1 + matrix2).ToString());
			Console.WriteLine("First - second:\n{0}", Matrix.Subtract(matrix1, matrix2).ToString());
			Console.WriteLine("First * third:\n{0}", (matrix1 * matrix3).ToString());
			Console.WriteLine("First * 2:\n{0}", (matrix1 * 2).ToString());
			Console.WriteLine("First transposed matrix:\n{0}", matrix1.Transpose().ToString());
			Console.WriteLine("Submatrix(3x4) from (1; 1) in first:\n{0}", matrix1.SubMatrix(1, 1, 3, 4).ToString());
			
			if (matrix1 == matrix4)
			{
				Console.WriteLine("First matrix and fourth matrix are equals");
			}
			if (matrix1 != matrix2)
			{
				Console.WriteLine("First matrix and second matrix are not equals");
			}
			if(matrix3.IsSquareMatrix())
			{
				Console.WriteLine("Third is square matrix");
			}
			if(matrix5.IsSymetricMatrix())
			{
				Console.WriteLine("Fifth is symetric matrix");
			}
			if(matrix5.IsZeroMatrix())
			{
				Console.WriteLine("Fifth is zero matrix");
			}
			
		}
	}
}