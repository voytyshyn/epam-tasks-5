// Second task
using System;
using System.Text;

namespace EpamTasks
{
	class Rectangle
	{
		private double _x;
		private double _y;
		private double _width;
		private double _height;
		
		public Rectangle()
		{
			_x = 0.0;
			_y = 0.0;
			_width = 0.0;
			_height = 0.0;
		}
		
		public Rectangle(double x, double y, double width, double height)
		{
			_x = x;
			_y = y;
			_width = width;
			_height = height;
		}
		
		public void ChangeLocation(double x, double y)
		{
			_x = x;
			_y = y;
		}
		
		public void ChangeSize(double width, double height)
		{
			_width = width;
			_height = height;
		}
		
		public void ChangeWidth(double width)
		{
			_width = width;
		}
		
		public void ChangeHeight(double height)
		{
			_height = height;
		}
		
		public static Rectangle Union(Rectangle rectangle1, Rectangle rectangle2)
		{
			double x1 = Math.Min(rectangle1._x, rectangle2._x);
            double x2 = Math.Max(rectangle1._x + rectangle1._width, rectangle2._x + rectangle2._width); 
            double y1 = Math.Max(rectangle1._y, rectangle2._y);
            double y2 = Math.Min(rectangle1._y - rectangle1._height, rectangle2._y - rectangle2._height);
 
            return new Rectangle(x1, y1, x2 - x1, y1 - y2); 
		}
		
		public static Rectangle Intersect(Rectangle rectangle1, Rectangle rectangle2) 
		{
            double x1 = Math.Max(rectangle1._x, rectangle2._x);
            double x2 = Math.Min(rectangle1._x + rectangle1._width, rectangle2._x + rectangle2._width); 
            double y1 = Math.Min(rectangle1._y, rectangle2._y);
            double y2 = Math.Max(rectangle1._y - rectangle1._height, rectangle2._y - rectangle2._height); 
  
            if (x2 >= x1 && y1 >= y2) 
			{ 
 
                return new Rectangle(x1, y1, x2 - x1, y1 - y2);
            }
            return new Rectangle(); 
        }
		
		public override string ToString()
		{
			return String.Format("({0}; {1}) width: {2}, height: {3}", _x, _y, _width, _height);
		}
	}
	
	class EntryPoint
	{
		static void Main()
		{
			Rectangle rec1 = new Rectangle(4, 2, 3, 2);
			Rectangle rec2 = new Rectangle(3, 5, 1, 2);
			Console.WriteLine("\n******************************SECOND TASK****************************\n");
			Console.WriteLine("First rectangle: {0}", rec1);
			Console.WriteLine("Second rectangle: {0}\n", rec2);
			
			rec1.ChangeLocation(1, 3);
			Console.WriteLine("First rectangle with changed location: {0}", rec1);
			rec2.ChangeSize(2, 3);
			Console.WriteLine("Second rectangle with changed size: {0}", rec2);
			Console.WriteLine("Union of both rectangles: {0}", Rectangle.Union(rec1, rec2));
			Console.WriteLine("Intersection of both rectangles: {0}", Rectangle.Intersect(rec1, rec2));
		}
	}
}